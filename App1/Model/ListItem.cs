﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.Model
{
    class ListItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public virtual List<TodoItem> Items { get; set; }
        public string Location { get; set; }
        public DateTime From { get; set; }
        public DateTime Till { get; set; }

    }
}
